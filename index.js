var SerialPort = require('serialport');
var address = require('address');
var request = require('request');
let mac = "";
const forFind = /#F..{11}/;
const forReplace = /#F..../;
const badForFind = /#S...+#S../;
let bufferString = "";
var port = new SerialPort('/dev/ttyUSB0', {
    baudRate: 9600,
    dataBits: 8,
    stopBits: 1,
    rtscts: true

});

function PostCode(form) {

    var headers = {
        'User-Agent':       'Super Agent/0.0.1',
        'Content-Type':     'application/x-www-form-urlencoded'
    }

// Configure the request
    var options = {
        url: 'http://10.10.10.15:3000/call',
        method: 'POST',
        headers: headers,
        form: form
    }

// Start the request
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            // Print out the response body
            console.log(body)
        }
    })

}

address.mac('eth0', function (err, addr) {
    mac = addr;
    // console.log(addr);
});

port.on('error', function(err) {
    // console.log('Error: ', err.message);
});
port.on('readable', function () {
    // console.log(port.read());
    var hexString = port.read();
    bufferString += hexString.toString();
    // console.log(hexString.toString());
    // console.log('without filters: ',bufferString);
    if(forFind.test(bufferString))
    {
        let cardId = parseInt(forFind.exec(bufferString)[0].replace(forReplace, "").toString(16), 16);
        // console.log('with filters ', bufferString);
        console.log('with regexp', parseInt(forFind.exec(bufferString)[0].replace(forReplace, "").toString(16), 16));
        // console.log(mac);
        var form = {mac: mac, cardId: cardId};
        PostCode(form);
        bufferString = "";
        //.replace(/\#F../, "")
    }
    else if(badForFind.test(bufferString))
    {
        bufferString = "";
    }
    // console.log(string);
});